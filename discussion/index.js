console.log("You are successfully connected!");
//SHOW POSTS
const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post)=>{
		console.log(post);
		postEntries += `
		<div id = "post-${post.id}">
			<h3 id = "post-title-${post.id}">${post.title}</h3>
			<p id = "post-body-${post.id}">${post.body}</p>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>

		</div>
		`
	})
	document.querySelector('#div-post-entries').innerHTML = postEntries;
};

/*
	fetch method
	Syntax:
		fetch('url', options)
		url - this is the url which the request is to be made

*/


fetch('https://jsonplaceholder.typicode.com/posts').then(response=> response.json()).then(data => showPosts(data));

//Add Post

document.querySelector('#form-add-post').addEventListener('submit',(e)=>{
	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts',{
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {
			"Content-Type":"application/json"
		}
	})
	.then(response => response.json())
	.then(data=>{
		console.log(data); //console pa lang, abangan
		alert("Post successfully added!")
		//auto clear
		document.querySelector('#txt-title').value=null;
		document.querySelector('#txt-body').value=null;
	})
});

//Edit Post
//populates the edit form
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
//once napopulate enable na yung button
	document.querySelector("#btn-submit-update").removeAttribute('disabled');
};


//Update Post
document.querySelector('#form-edit-post').addEventListener("submit",(e) =>{
	e.preventDefault();
	fetch('https://jsonplaceholder.typicode.com/posts/1',{
		method: 'PUT',
		//stringify first

		body: JSON.stringify({
			id: document.querySelector("#txt-edit-id").value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1 
		}),
		headers: {
			"Content-Type":"application/json"
		}
	})
	.then(response=>response.json())
	.then(data=>{
		console.log(data)
		alert("Post successfully updated");
		//mini activity
		document.querySelector('#txt-edit-title').value=null;
		document.querySelector('#txt-edit-body').value=null;
		document.querySelector("#btn-submit-update").setAttribute('disabled','');//truthy ang '' yey
	})
});


//test
// const deletePost = (id) => {
// document.querySelector(`#post-${id}`).remove();
// }


const deletePost = (id) => {
document.querySelector(`#post-${id}`).remove()

	fetch('https://jsonplaceholder.typicode.com/posts/1',{
		method: 'DELETE'
	})
	.then(response=>response.json())
	.then(data=>{
		console.log(data)
		alert("Post deleted.");
	})

}

// document.querySelector('#div-post-entries').addEventListener("submit",(e) =>{
// 	e.preventDefault();

// });